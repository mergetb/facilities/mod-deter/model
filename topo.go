package moddeter

import (
	"fmt"
	"log"

	xir "gitlab.com/mergetb/xir/v0.3/go"
	. "gitlab.com/mergetb/xir/v0.3/go/build"
)

// Define a set of global variables for easy access to various testbed elements.
var (
	nodes               []*xir.Resource
	mleaf, ileaf, xleaf *xir.Resource
	infrapods           []*xir.Resource
	ops                 *xir.Resource
	mflat               *xir.Resource
)

func Topo() *xir.Facility {

	// Create a builder object to construct the testbed with. This is an object
	// that provides convenience functions creating all the components,
	// subcomponents and cabling connections that go into constructing a testbed
	// and provides an interface for creating a serializable XIR model from
	// those components and connections.
	tb, err := NewBuilder(
		"moddeter",
		"facility.mod.deterlab.net",
	)
	if err != nil {
		log.Fatalf("new builder: %v", err)
	}
	// ------------------------v switches v--------------------------------

	// Flat switch to extend mgmt network.
	mflat = tb.MgmtLeaf("ml252r207s14-netgear",
		Eth(32, Gbps(1), Mgmt()),
		Product("NetGear", "ProSAFE 32 port 10/100/1000 Mbps", "JFS524E"),
	)
	// Management switch
	mleaf = tb.MgmtLeaf("ml252r207s12-mleaf",
		Eth(1, Gbps(1)),
		Swp(48, Gbps(1), Mgmt()),
		// xir 0.3.5 Swp(4, Gbps(10), StartingIndex(49)),
		Swp(4, Gbps(10)),
		Product("EdgeCore", "52 port 1G/10G switch", "AS4610-54T-O-AC-F v1"),
	)
	mleaf.Mgmt().Mac = "68:21:5f:f1:7e:40"

	// Infranet switch
	ileaf = tb.InfraLeaf("il252r207s10-ileaf",
		Eth(1, Gbps(1), Mgmt()),
		Swp(48, Gbps(25), Infranet()),
		// xir 0.3.5 Swp(12, Gbps(100), StartingIndex(49)),
		Swp(12, Gbps(100), Infranet()),
		Product("Mellanox", "48x25gbps + 12x100gbps Spectrum-2 switch", "MSN3420-CB2FC"),
		RoleAdd(xir.Role_Stem),
	)
	ileaf.NICs[0].Ports[0].Mac = "b8:ce:f6:20:a0:50"
	ileaf.NICs[2].Ports[11].Mac = "1c:34:da:cf:04:40"

	// Experiment switch
	xleaf = tb.XpLeaf("xl252r207s8-xleaf",
		Eth(1, Gbps(1), Mgmt()),
		Swp(64, Gbps(100), Xpnet()),
		Product("Mellanox", "64x 100gbps Spectrum-3 switch", "MSN4600-CS2RC"),
		RoleAdd(xir.Role_Stem),
	)
	xleaf.Mgmt().Mac = "08:c0:eb:20:5c:42"

	// -------------------^ end  switches ^--------------------------------
	// ------------------v infrastructure hosts v--------------------------
	// Ops server
	ops = tb.OpsServer("ops",
		Procs(1, Cores(16), Product("AMD", "16C 3.0Ghz 128mb 3200Mhz 155W", "Epyc 7313P")),
		Dimms(16, GB(16), Product("Gigabyte", "Platform Certified 16GB Memory module", "GB16")),
		NVMEs(2, 0, GB(480), Product("Micron", "480GB M.2 NVMe x4 22x80mm", "M7300")),
		NVMEs(2, 1, GB(3200), Product("Kioxia", "CD6-V 3.2TB 3DWPD U.2 Gen4 NVMe", "KCD61VUL3T20")),
		Ipmi(1, Mbps(100)),
		Uefi(),
		Eno(1, Gbps(1), Mgmt()),
		Eno(2, Gbps(1)),
		Enp(4, 1, Gbps(25), Infranet(), SFP28(), Product("Intel", "Quad Port 10/25gbps PCIe 4.0x16", "E810-XXVDA4")),
		Product("Gigabyte", "Custom Ops Server", "OPSSERVER/CX1165g-NVMe4-E8"),
	)
	ops.Mgmt().Mac = "b4:96:91:a5:01:04"
	ops.NICs[3].Ports[0].Name = "enp193s0f0"
	ops.NICs[3].Ports[1].Name = "enp193s0f1"
	ops.NICs[3].Ports[2].Name = "enp193s0f2"
	ops.NICs[3].Ports[3].Name = "enp193s0f3"

	// Infrapod server
	infrapods = tb.Infraservers(2, "ifr",
		Procs(2, Cores(32), Product("AMD", "32C 2.8Ghz 256mb 3200Mhz 225W", "Epyc 7543")),
		Dimms(32, GB(16), Product("Gigabyte", "Platform Certified 16GB Memory module", "GB16")),
		// do not specify etcdisk and miniodisk -- patch those into ignition later
		NVMEs(1, 0, GB(4000), SysDisk() /*U2(),*/, Product("Intel", "4TB U.2 15mm NVMe", "DC P4510-4000")),
		NVMEs(1, 1, GB(4000), MinioDisk() /*U2(),*/, Product("Intel", "4TB U.2 15mm NVMe", "DC P4510-4000")),
		NVMEs(1, 2, GB(1024), EtcdDisk() /*M2_22110(),*/, Product("Samsung", "960GB M.2 NVMex4 22x110mm", "PM9A3")),
		Ipmi(1, Mbps(100)),
		BMCIpmi(), Uefi(),
		Eno(2, Gbps(1), Mgmt()),
		Enp(2, 1, Gbps(100), Infranet(1), Gw(0), QSFP28(), Product("Mellanox", "Dual port 100gbps ConnectX6", "MCX653106A-ECAT")),
		Product("Gigabyte", "Custom Infrapod Server", "INFRASERVER/CX1265g-NVMe-E8"),
		Rootdev("/dev/nvme0n1p4"),
		RoleAdd(xir.Role_BorderGateway),
		RoleAdd(xir.Role_Gateway), // List Of Materials (./model lom) can be broken by this role
		RoleAdd(xir.Role_EtcdHost),
		RoleAdd(xir.Role_MinIOHost),
		RoleAdd(xir.Role_SledHost),
		RoleAdd(xir.Role_InfrapodServer),
	)

	// nic0,port0 is ipmi.
	infrapods[0].NICs[0].Ports[0].Mac = "d8:5e:d3:69:38:02"
	infrapods[0].Mgmt().Mac = "d8:5e:d3:69:38:00"
	// ingress port on ileaf
	infrapods[0].NICs[2].Ports[0].Mac = "08:c0:eb:af:43:7a"
	infrapods[0].NICs[2].Ports[0].Name = "enp33s0f0np0"
	// "gateway" port on ileaf
	infrapods[0].NICs[2].Ports[1].Mac = "08:c0:eb:af:43:7b"
	infrapods[0].NICs[2].Ports[1].Name = "enp33s0f1np1"

	infrapods[1].NICs[0].Ports[0].Mac = "d8:5e:d3:69:37:c2"
	infrapods[1].Mgmt().Mac = "d8:5e:d3:69:37:c0"
	infrapods[1].NICs[2].Ports[0].Mac = "08:c0:eb:af:42:6a"
	infrapods[1].NICs[2].Ports[0].Name = "enp33s0f0np0"
	infrapods[1].NICs[2].Ports[1].Mac = "08:c0:eb:af:42:6b"
	infrapods[1].NICs[2].Ports[1].Name = "enp33s0f1np1"
	// --------------^ end infrastructure hosts ^--------------------------
	// ------------------v  nodes  v---------------------------------------

	/* cant use this, due to static emulators
	nodes = tb.Nodes(48, "md",
		Procs(2, Cores(64), Product("AMD", "64C 2.0Ghz 256mb 200W", "Epyc 7702")),
		Dimms(16, GB(16), Product("Gigabyte", "Platform Certified 16GB Memory module", "GB16")),
		NVMEs(1, 0, GB(1024), Product("Intel", "1024GB M.2 NVMe x4 22x110mm", "DC P4511")),
		NVMEs(6, 1, GB(1600), Product("Kioxia", "1.6TB 3DWPD U.2 Gen4 NVMe", "CD6-V-1600")),
		Ipmi(1, Mbps(1000)),
		Eno(2, Gbps(1), Mgmt()),
		Enp(2, 1, Gbps(25), Infranet(), Product("Intel", "Dual Port 25gbps OCP 3.0", "E810-XXVDA2")),
		Enp(2, 1, Gbps(100), Xpnet(), Product("Intel", "Dual Port 100gbps PCIe 4.0 x 16", "E810-CQDA2")),

		Product("Gigabyte", "Custom Mod-Deter Server", "MDSERVER/CX22885g-NVMe4-E8"),
	)
	*/
	for i := 0; i < 48; i++ {
		if i == 0 { // static emulators for v1 of merge
			modes := []xir.AllocMode{}
			nodes = append(nodes, tb.NetworkEmulator( // statically allocated emulator node
				fmt.Sprintf("md%02d", i),
				Procs(2, Cores(64), Product("AMD", "64C 2.0Ghz 256mb 200W", "Epyc 7702")),
				Dimms(16, GB(16), Product("Gigabyte", "Platform Certified 16GB Memory module", "GB16")),
				NVMEs(1, 0, GB(1024), SysDisk(), Product("Micron", "7400Max 800GB 3DWPD M.2 NVMe x4 22x110mm", "MTFDKBA800TFC-1AZ1ZABYY")),
				NVMEs(6, 1, GB(1600), Product("Kioxia", "CD6-V 1.6TB 3DWPD U.2 NVMe x4", "KCD61VUL1T60")),
				Ipmi(1, Mbps(1000)),
				BMCIpmi(), Uefi(),
				AllocModes(modes...),
				Eno(2, Gbps(1), Mgmt()),
				Enp(2, 1, Gbps(25), Infranet(), Product("Mellanox", "CX4 Dual Port 25gbps PCIe4.0 x8", "MCX4121A-ACAT")),
				Enp(2, 1, Gbps(100), Xpnet(), Product("Mellanox", "CX6 Dual Port 100gbps PCIe4.0 x16", "MCX653106A-ECAT")),
				Product("Gigabyte", "Custom Mod-Deter Server", "MDSERVER/CX22885g-NVMe4-E8"),
				Rootdev("/dev/nvme0n1"),
				DefaultImage("bullseye"),
			))
		} else {
			modes := []xir.AllocMode{
				xir.AllocMode_Physical,
				xir.AllocMode_Virtual,
			}
			nodes = append(nodes, tb.Node(
				fmt.Sprintf("md%02d", i),
				// reserved 1 core on each procs for OS use
				Procs(2, Cores(64), Reserved(1), Product("AMD", "64C 2.0Ghz 256mb 200W", "Epyc 7702")),
				// reserved 100% of 2 16GB DIMM for OS use due to overprovisioning issues
				Dimms(2, GB(16), Reserved(xir.GB(16)), Product("Gigabyte", "Platform Certified 16GB Memory module", "GB16")),
				Dimms(14, GB(16), Product("Gigabyte", "Platform Certified 16GB Memory module", "GB16")),
				NVMEs(1, 0, GB(1024), SysDisk(), Product("Micron", "7400Max 800GB 3DWPD M.2 NVMe x4 22x80mm", "MTFDKBA800TFC-1AZ1ZABYY")),
				NVMEs(6, 1, GB(1600), MarinerDisk(), Product("Kioxia", "CD6-V 1.6TB 3DWPD U.2 NVMe x4", "KCD61VUL1T60")),
				Ipmi(1, Mbps(1000)),
				BMCIpmi(), Uefi(),
				AllocModes(modes...),
				Roles(
					xir.Role_TbNode,
					xir.Role_Hypervisor,
				),
				Eno(2, Gbps(1), Mgmt()),
				Enp(2, 1, Gbps(25), Infranet(), Product("Mellanox", "CX4 Dual Port 25gbps PCIe4.0 x8", "MCX4121A-ACAT")),
				Enp(2, 1, Gbps(100), Xpnet(), Product("Mellanox", "CX6 Dual Port 100gbps PCIe4.0 x16", "MCX653106A-ECAT")),
				Product("Gigabyte", "Custom Mod-Deter Server", "MDSERVER/CX22885g-NVMe4-E8"),
				Rootdev("/dev/nvme0n1"),
				DefaultImage("bullseye"),
			))
		}
		nodes[i].NICs[2].Ports[0].Name = "enp65s0f0np0"
		nodes[i].NICs[2].Ports[1].Name = "enp65s0f1np1"
		nodes[i].NICs[3].Ports[0].Name = "enp33s0f0np0"
		nodes[i].NICs[3].Ports[1].Name = "enp33s0f1np1"
	}

	// We start off models with "ideal" cabling. We do this because we do not
	// know in advance if what seems reasonable on paper as a cabling strategy
	// will also be reasonable in real physical 3 dimensional space. There are
	// often alterations that can be made to make cabling much more manageable
	// and maintainable, so once the testbed is actually constructed we
	// typically add a `actualCabling` function and call it here in place of
	// idealCabling.
	actualCabling(tb)
	//idealCabling(tb)
	nodemacs(tb)
	return tb.Facility()

}

func actualCabling(tb *Builder) {
	// Ops connections
	tb.BreakoutTrunk(
		ileaf.NICs[2],
		ileaf.Swp(58),
		ops.NextEnpsG(25, 4),
		"opsbond",
		"ileaf",
		QSFP28_4xSFP28(),
		Product("FS", "100-4x25 MTP4-LC Duplex OM4 Breakout 3'", "QSFP28-SR4-100G/8FMTPLCOM4/SFP28-25GSR-85x4"),
	)
	tb.Connect(ops.NextEno(), mflat.Eth(11), RJ45(), Product("Generic", "Cat6 RJ45 Cable", "GENERIC_CAT6"))

	// switches connections
	tb.Connect(mleaf.Eth(0), mflat.Eth(2))
	tb.BreakoutTrunk(
		ileaf.NICs[2],
		ileaf.Swp(56),
		mleaf.NextSwpsG(4, 10),
		"mleaf",
		"ileaf",
		QSFP28_4xSFP28(),
		Product("FS", "100-4x25 MTP4-LC Duplex OM4 Breakout 3'", "QSFP28-SR4-100G/8FMTPLCOM4/SFP28-25GSR-85x4"),
	)
	// infra leaf management connection
	tb.Connect(ileaf.Mgmt(), mflat.Eth(23))
	// Experiment switch management connection
	tb.Connect(xleaf.Mgmt(), mflat.Eth(24))

	// Infrapod connections
	// infrapod 1 is connected but not powered
	// mangement network	// connected to ops via dumb switch
	tb.Connect(infrapods[0].Mgmt(), mflat.Eth(12), RJ45(), Product("Generic", "Cat6 RJ45 Cable", "GENERIC_CAT6"))
	tb.Connect(infrapods[1].Mgmt(), mflat.Eth(19), RJ45(), Product("Generic", "Cat6 RJ45 Cable", "GENERIC_CAT6"))
	// infranet -- cannot trunk, need disparate interfaces
	////    for the GW network and INFRA network.
	// here, we are using NextEnpG and NextSwpG because we will use swps 49+ and all 100G ENPs.
	//// feel free to adjust to use "real" ports/numbers
	//tb.Connect(infrapods[0].NextEnpG(100), ileaf.NextSwpG(100), QSFP28(), Product("FS", "100gbps fiber", "QSFP28-SR4-100G/12FMTPOM4"))
	tb.Connect(infrapods[0].NextEnpG(100), ileaf.Swp(49), QSFP28(), Product("FS", "100gbps fiber", "QSFP28-SR4-100G/12FMTPOM4"))
	//tb.Connect(infrapods[0].NextEnpG(100), ileaf.NextSwpG(100), QSFP28(), Product("FS", "100gbps fiber", "QSFP28-SR4-100G/12FMTPOM4"))
	tb.Connect(infrapods[0].NextEnpG(100), ileaf.Swp(48), QSFP28(), Product("FS", "100gbps fiber", "QSFP28-SR4-100G/12FMTPOM4"))
	tb.Connect(infrapods[1].NextEnpG(100), ileaf.NextSwpG(100), QSFP28(), Product("FS", "100gbps fiber", "QSFP28-SR4-100G/12FMTPOM4"))
	tb.Connect(infrapods[1].NextEnpG(100), ileaf.NextSwpG(100), QSFP28(), Product("FS", "100gbps fiber", "QSFP28-SR4-100G/12FMTPOM4"))

	// Node connections -- these should remain very similar to ideal, with the exception
	// ** node number 0 is going to be an emulator node.
	for _, x := range nodes {
		// management network
		tb.Connect(
			x.Mgmt(),
			mleaf.NextSwp(),
			RJ45(),
			Product("Generic", "Cat6 RJ45 Cable", "GENERIC_CAT6"),
		)
		// infranet
		tb.Connect(
			x.Infranet(),
			ileaf.NextSwpG(25),
			SFP28(),
			Product("FS", "25gbps fiber", "SFP28-25GSR-85/OM4-LC-LC-DX-FS-2M-PVC"),
		)
		// until we have dynamic emulator nodes, we will be using md0 as the emulator,
		// and if it's the only emulator, we really want 200Gbps if possible.
		if x == nodes[0] {
			// emulator xpnet -- nxtswp should be port swp1
			tb.Trunk(
				x.NICs[3].Ports[0:2],
				[]*xir.Port{
					xleaf.NextSwp(),
					xleaf.Swp(48),
				},
				"xleaf",
				x.Id,
				QSFP28(),
				Product("FS", "100gbps fiber", "QSFP28-SR4-100G/12FMTPOM4"),
			)
		} else {
			// xpnet
			tb.Connect(
				x.NICs[3].Ports[0],
				xleaf.NextSwp(),
				QSFP28(),
				Product("FS", "100gbps fiber", "QSFP28-SR4-100G/12FMTPOM4"),
			)
		}
	}
}

func nodemacs(tb *Builder) {
	//md00 (emulator) spreadsheet chassis4,node1
	nodes[0].NICs[0].Ports[0].Mac = "18:c0:4d:b7:6d:79"
	nodes[0].Mgmt().Mac = "18:c0:4d:b7:6d:76"
	nodes[0].Infranet().Mac = "10:70:fd:57:0e:24"
	nodes[0].NICs[3].Ports[0].Mac = "08:c0:eb:ee:68:06"
	nodes[0].NICs[3].Ports[1].Mac = "08:c0:eb:ee:68:07"
	//md01! ch4,n2
	nodes[1].NICs[0].Ports[0].Mac = "18:c0:4d:b7:6c:dd"
	nodes[1].Mgmt().Mac = "18:c0:4d:b7:6c:da"
	nodes[1].Infranet().Mac = "10:70:fd:57:0e:58"
	nodes[1].NICs[3].Ports[0].Mac = "08:c0:eb:ee:60:ae"
	//md02! ch4,n3
	nodes[2].NICs[0].Ports[0].Mac = "18:c0:4d:b7:6d:5d"
	nodes[2].Mgmt().Mac = "18:c0:4d:b7:6d:5a"
	nodes[2].Infranet().Mac = "10:70:fd:51:82:0c"
	nodes[2].NICs[3].Ports[0].Mac = "08:c0:eb:ee:65:8e"
	//md03! ch4,n4
	nodes[3].NICs[0].Ports[0].Mac = "18:c0:4d:b7:6d:4d"
	nodes[3].Mgmt().Mac = "18:c0:4d:b7:6d:4a"
	nodes[3].Infranet().Mac = "10:70:fd:57:0e:2c"
	nodes[3].NICs[3].Ports[0].Mac = "08:c0:eb:ee:66:a6"
	//md04! ch5,n1
	nodes[4].NICs[0].Ports[0].Mac = "18:c0:4d:b7:6c:b9"
	nodes[4].Mgmt().Mac = "18:c0:4d:b7:6c:b6"
	nodes[4].Infranet().Mac = "10:70:fd:57:0e:10"
	nodes[4].NICs[3].Ports[0].Mac = "08:c0:eb:ee:66:f6"
	//md05! ch5,n2
	nodes[5].NICs[0].Ports[0].Mac = "18:c0:4d:b7:6c:cd"
	nodes[5].Mgmt().Mac = "18:c0:4d:b7:6c:ca"
	nodes[5].Infranet().Mac = "10:70:fd:57:0d:e0"
	nodes[5].NICs[3].Ports[0].Mac = "08:c0:eb:ee:65:6e"
	//md06! ch5,n3
	nodes[6].NICs[0].Ports[0].Mac = "18:c0:4d:b7:6c:d1"
	nodes[6].Mgmt().Mac = "18:c0:4d:b7:6c:ce"
	nodes[6].Infranet().Mac = "10:70:fd:57:0e:0c"
	nodes[6].NICs[3].Ports[0].Mac = "08:c0:eb:ee:5f:66"
	//md07! ch5,n4
	nodes[7].NICs[0].Ports[0].Mac = "18:c0:4d:b7:6c:e5"
	nodes[7].Mgmt().Mac = "18:c0:4d:b7:6c:e2"
	nodes[7].Infranet().Mac = "10:70:fd:57:0d:d8"
	nodes[7].NICs[3].Ports[0].Mac = "08:c0:eb:ee:67:06"
	//md08! ch6,n1
	nodes[8].NICs[0].Ports[0].Mac = "18:c0:4d:b7:6d:89"
	nodes[8].Mgmt().Mac = "18:c0:4d:b7:6d:86"
	nodes[8].Infranet().Mac = "10:70:fd:51:8a:78"
	nodes[8].NICs[3].Ports[0].Mac = "08:c0:eb:ee:64:3e"
	//md09! ch6,n2
	nodes[9].NICs[0].Ports[0].Mac = "18:c0:4d:8b:2b:e4"
	nodes[9].Mgmt().Mac = "18:c0:4d:8b:2b:e1"
	nodes[9].Infranet().Mac = "10:70:fd:57:0d:a8"
	nodes[9].NICs[3].Ports[0].Mac = "08:c0:eb:ee:63:56"
	//md10! ch6,n3
	nodes[10].NICs[0].Ports[0].Mac = "18:c0:4d:b7:6d:a1"
	nodes[10].Mgmt().Mac = "18:c0:4d:b7:6d:9e"
	nodes[10].Infranet().Mac = "10:70:fd:51:8a:70"
	nodes[10].NICs[3].Ports[0].Mac = "08:c0:eb:ee:63:f6"
	//md11! ch6,n4
	nodes[11].NICs[0].Ports[0].Mac = "18:c0:4d:b7:6c:c5"
	nodes[11].Mgmt().Mac = "18:c0:4d:b7:6c:c2"
	nodes[11].Infranet().Mac = "10:70:fd:57:0d:80"
	nodes[11].NICs[3].Ports[0].Mac = "08:c0:eb:ee:63:36"
	//md12@ ch3,n1?
	nodes[12].NICs[0].Ports[0].Mac = "18:c0:4d:b7:6d:15"
	nodes[12].Mgmt().Mac = "18:c0:4d:b7:6d:12"
	nodes[12].Infranet().Mac = "10:70:fd:57:0e:14"
	nodes[12].NICs[3].Ports[0].Mac = "08:c0:eb:ee:63:4e"
	//md13@ ch3,n2?
	nodes[13].NICs[0].Ports[0].Mac = "10:ff:e0:0e:cc:ac"
	nodes[13].Mgmt().Mac = "10:ff:e0:0e:cc:af"
	nodes[13].Infranet().Mac = "10:70:fd:57:0e:30"
	nodes[13].NICs[3].Ports[0].Mac = "08:c0:eb:ee:65:1e"
	//md14@ ch3,n3?
	nodes[14].NICs[0].Ports[0].Mac = "18:c0:4d:b7:6d:59"
	nodes[14].Mgmt().Mac = "18:c0:4d:b7:6d:56"
	nodes[14].Infranet().Mac = "10:70:fd:57:0e:20"
	nodes[14].NICs[3].Ports[0].Mac = "08:c0:eb:ee:63:6e"
	//md15@ ch3,n4?
	nodes[15].NICs[0].Ports[0].Mac = "18:c0:4d:b7:6d:2d"
	nodes[15].Mgmt().Mac = "18:c0:4d:b7:6d:2a"
	nodes[15].Infranet().Mac = "10:70:fd:57:0d:ec"
	nodes[15].NICs[3].Ports[0].Mac = "08:c0:eb:ee:63:76"
	//md16! ch2,n1
	nodes[16].NICs[0].Ports[0].Mac = "18:c0:4d:b7:6c:d9"
	nodes[16].Mgmt().Mac = "18:c0:4d:b7:6c:d6"
	nodes[16].Infranet().Mac = "10:70:fd:57:0e:1c"
	nodes[16].NICs[3].Ports[0].Mac = "08:c0:eb:ee:65:5e"
	//md17! ch2,n2
	nodes[17].NICs[0].Ports[0].Mac = "18:c0:4d:b7:6c:bd"
	nodes[17].Mgmt().Mac = "18:c0:4d:b7:6c:ba"
	nodes[17].Infranet().Mac = "10:70:fd:57:0d:f8"
	nodes[17].NICs[3].Ports[0].Mac = "08:c0:eb:ee:63:fe"
	//md18! ch2,n3
	nodes[18].NICs[0].Ports[0].Mac = "18:c0:4d:b7:6c:a1"
	nodes[18].Mgmt().Mac = "18:c0:4d:b7:6c:9e"
	nodes[18].Infranet().Mac = "10:70:fd:57:0e:28"
	nodes[18].NICs[3].Ports[0].Mac = "08:c0:eb:ee:63:46"
	//md19! ch2,n4
	nodes[19].NICs[0].Ports[0].Mac = "18:c0:4d:b2:6c:9b"
	nodes[19].Mgmt().Mac = "18:c0:4d:b2:6c:98"
	nodes[19].Infranet().Mac = "10:70:fd:57:0e:08"
	nodes[19].NICs[3].Ports[0].Mac = "08:c0:eb:ee:67:2e"
	//md20! ch1,n1
	nodes[20].NICs[0].Ports[0].Mac = "18:c0:4d:b7:6c:9d"
	nodes[20].Mgmt().Mac = "18:c0:4d:b7:6c:9a"
	nodes[20].Infranet().Mac = "10:70:fd:57:0d:fc"
	nodes[20].NICs[3].Ports[0].Mac = "08:c0:eb:ee:66:ee"
	//md21! ch1,n2
	nodes[21].NICs[0].Ports[0].Mac = "18:c0:4d:b7:6c:ad"
	nodes[21].Mgmt().Mac = "18:c0:4d:b7:6c:aa"
	nodes[21].Infranet().Mac = "10:70:fd:57:0e:04"
	nodes[21].NICs[3].Ports[0].Mac = "08:c0:eb:ee:65:56"
	//md22! ch1,n3
	nodes[22].NICs[0].Ports[0].Mac = "18:c0:4d:7a:58:a5"
	nodes[22].Mgmt().Mac = "18:c0:4d:7a:58:a2"
	nodes[22].Infranet().Mac = "10:70:fd:57:0e:68"
	nodes[22].NICs[3].Ports[0].Mac = "08:c0:eb:ee:65:66"
	//md23! ch1,n4
	nodes[23].NICs[0].Ports[0].Mac = "18:c0:4d:b7:6c:f5"
	nodes[23].Mgmt().Mac = "18:c0:4d:b7:6c:f2"
	nodes[23].Infranet().Mac = "10:70:fd:57:0e:18"
	nodes[23].NICs[3].Ports[0].Mac = "08:c0:eb:ee:67:46"
	//md24! ch8,n1
	nodes[24].NICs[0].Ports[0].Mac = "18:c0:4d:b7:6d:ad"
	nodes[24].Mgmt().Mac = "18:c0:4d:b7:6d:aa"
	nodes[24].Infranet().Mac = "10:70:fd:57:0d:8c"
	nodes[24].NICs[3].Ports[0].Mac = "08:c0:eb:ee:66:be"
	//md25! ch8,n2
	nodes[25].NICs[0].Ports[0].Mac = "18:c0:4d:b7:6d:75"
	nodes[25].Mgmt().Mac = "18:c0:4d:b7:6d:72"
	nodes[25].Infranet().Mac = "10:70:fd:57:0d:74"
	nodes[25].NICs[3].Ports[0].Mac = "08:c0:eb:ee:67:3e"
	//md26! ch8,n3
	nodes[26].NICs[0].Ports[0].Mac = "18:c0:4d:b7:6d:01"
	nodes[26].Mgmt().Mac = "18:c0:4d:b7:6c:fe"
	nodes[26].Infranet().Mac = "10:70:fd:51:8a:68"
	nodes[26].NICs[3].Ports[0].Mac = "08:c0:eb:ee:64:2e"
	//md27! ch8,n4
	nodes[27].NICs[0].Ports[0].Mac = "18:c0:4d:8d:96:be"
	nodes[27].Mgmt().Mac = "18:c0:4d:8d:96:bb"
	nodes[27].Infranet().Mac = "10:70:fd:51:8a:44"
	nodes[27].NICs[3].Ports[0].Mac = "08:c0:eb:ee:62:3e"
	//md28! ch7,n1
	nodes[28].NICs[0].Ports[0].Mac = "18:c0:4d:77:8e:bb"
	nodes[28].Mgmt().Mac = "18:c0:4d:77:8e:b8"
	nodes[28].Infranet().Mac = "10:70:fd:57:0d:9c"
	nodes[28].NICs[3].Ports[0].Mac = "08:c0:eb:ee:64:46"
	//md29! ch7,n2
	nodes[29].NICs[0].Ports[0].Mac = "18:c0:4d:b7:6d:c9"
	nodes[29].Mgmt().Mac = "18:c0:4d:b7:6d:c6"
	nodes[29].Infranet().Mac = "10:70:fd:57:0d:ac"
	nodes[29].NICs[3].Ports[0].Mac = "08:c0:eb:ee:64:36"
	//md30! ch7,n3
	nodes[30].NICs[0].Ports[0].Mac = "18:c0:4d:77:8e:0f"
	nodes[30].Mgmt().Mac = "18:c0:4d:77:8e:0c"
	nodes[30].Infranet().Mac = "10:70:fd:51:8a:8c"
	nodes[30].NICs[3].Ports[0].Mac = "08:c0:eb:ee:64:1e"
	//md31! ch7,n4
	nodes[31].NICs[0].Ports[0].Mac = "18:c0:4d:8b:2c:b4"
	nodes[31].Mgmt().Mac = "18:c0:4d:8b:2c:b1"
	nodes[31].Infranet().Mac = "10:70:fd:57:0d:b8"
	nodes[31].NICs[3].Ports[0].Mac = "08:c0:eb:ee:64:16"
	//md32! ch9,n1
	nodes[32].NICs[0].Ports[0].Mac = "18:c0:4d:b7:6c:f1"
	nodes[32].Mgmt().Mac = "18:c0:4d:b7:6c:ee"
	nodes[32].Infranet().Mac = "10:70:fd:57:0d:a4"
	nodes[32].NICs[3].Ports[0].Mac = "08:c0:eb:ee:66:ce"
	//md33! ch9,n2
	nodes[33].NICs[0].Ports[0].Mac = "18:c0:4d:b7:6c:b1"
	nodes[33].Mgmt().Mac = "18:c0:4d:b7:6c:ae"
	nodes[33].Infranet().Mac = "10:70:fd:57:0d:7c"
	nodes[33].NICs[3].Ports[0].Mac = "08:c0:eb:ee:5f:6e"
	//md34! ch9,n3
	nodes[34].NICs[0].Ports[0].Mac = "18:c0:4d:b7:6c:99"
	nodes[34].Mgmt().Mac = "18:c0:4d:b7:6c:96"
	nodes[34].Infranet().Mac = "10:70:fd:57:0d:98"
	nodes[34].NICs[3].Ports[0].Mac = "08:c0:eb:ee:5f:5e"
	//md35! ch9,n4
	nodes[35].NICs[0].Ports[0].Mac = "18:c0:4d:b7:6c:e9"
	nodes[35].Mgmt().Mac = "18:c0:4d:b7:6c:e6"
	nodes[35].Infranet().Mac = "10:70:fd:57:0d:cc"
	nodes[35].NICs[3].Ports[0].Mac = "08:c0:eb:ee:66:fe"
	//md36@ ch10,n1?
	nodes[36].NICs[0].Ports[0].Mac = "18:c0:4d:b7:6d:29"
	nodes[36].Mgmt().Mac = "18:c0:4d:b7:6d:26"
	nodes[36].Infranet().Mac = "10:70:fd:45:49:74"
	nodes[36].NICs[3].Ports[0].Mac = "08:c0:eb:ee:64:26"
	//md37@ ch10,n2?
	nodes[37].NICs[0].Ports[0].Mac = "18:c0:4d:b7:6d:35"
	nodes[37].Mgmt().Mac = "18:c0:4d:b7:6d:32"
	nodes[37].Infranet().Mac = "10:70:fd:45:49:90"
	nodes[37].NICs[3].Ports[0].Mac = "08:c0:eb:ee:64:06"
	//md38@ ch10,n3?
	nodes[38].NICs[0].Ports[0].Mac = "18:c0:4d:8c:80:2c"
	nodes[38].Mgmt().Mac = "18:c0:4d:8c:80:29"
	nodes[38].Infranet().Mac = "10:70:fd:45:49:20"
	nodes[38].NICs[3].Ports[0].Mac = "08:c0:eb:ee:68:76"
	//md39@ ch10,n4?
	nodes[39].NICs[0].Ports[0].Mac = "18:c0:4d:8d:96:c6"
	nodes[39].Mgmt().Mac = "18:c0:4d:8d:96:c3"
	nodes[39].Infranet().Mac = "10:70:fd:57:0e:3c"
	nodes[39].NICs[3].Ports[0].Mac = "08:c0:eb:ee:68:f6"
	//md40! ch11,n1
	nodes[40].NICs[0].Ports[0].Mac = "18:c0:4d:b7:6d:19"
	nodes[40].Mgmt().Mac = "18:c0:4d:b7:6d:16"
	nodes[40].Infranet().Mac = "10:70:fd:51:8a:84"
	nodes[40].NICs[3].Ports[0].Mac = "08:c0:eb:ee:68:ae"
	//md41! ch11,n2
	nodes[41].NICs[0].Ports[0].Mac = "18:c0:4d:b7:6d:c5"
	nodes[41].Mgmt().Mac = "18:c0:4d:b7:6d:c2"
	nodes[41].Infranet().Mac = "10:70:fd:51:8a:6c"
	nodes[41].NICs[3].Ports[0].Mac = "08:c0:eb:ee:65:d6"
	//md42! ch11,n3
	nodes[42].NICs[0].Ports[0].Mac = "18:c0:4d:b7:6d:a5"
	nodes[42].Mgmt().Mac = "18:c0:4d:b7:6d:a2"
	nodes[42].Infranet().Mac = "10:70:fd:45:49:ec"
	nodes[42].NICs[3].Ports[0].Mac = "08:c0:eb:ee:68:26"
	//md43! ch11,n4
	nodes[43].NICs[0].Ports[0].Mac = "18:c0:4d:8b:2b:98"
	nodes[43].Mgmt().Mac = "18:c0:4d:8b:2b:95"
	nodes[43].Infranet().Mac = "10:70:fd:45:4a:00"
	nodes[43].NICs[3].Ports[0].Mac = "08:c0:eb:ee:68:1e"
	//md44! ch12,n1
	nodes[44].NICs[0].Ports[0].Mac = "18:c0:4d:81:7b:9f"
	nodes[44].Mgmt().Mac = "18:c0:4d:81:7b:9c"
	nodes[44].Infranet().Mac = "10:70:fd:45:49:18"
	nodes[44].NICs[3].Ports[0].Mac = "08:c0:eb:ee:68:6e"
	//md45! ch12,n2
	nodes[45].NICs[0].Ports[0].Mac = "18:c0:4d:b7:6c:b5"
	nodes[45].Mgmt().Mac = "18:c0:4d:b7:6c:b2"
	nodes[45].Infranet().Mac = "10:70:fd:45:49:f8"
	nodes[45].NICs[3].Ports[0].Mac = "08:c0:eb:ee:63:66"
	//md46! ch12,n3
	nodes[46].NICs[0].Ports[0].Mac = "18:c0:4d:7a:58:6d"
	nodes[46].Mgmt().Mac = "18:c0:4d:7a:58:6a"
	nodes[46].Infranet().Mac = "10:70:fd:45:49:a0"
	nodes[46].NICs[3].Ports[0].Mac = "08:c0:eb:ee:68:2e"
	//md47! ch12,n4
	nodes[47].NICs[0].Ports[0].Mac = "18:c0:4d:b7:6c:c1"
	nodes[47].Mgmt().Mac = "18:c0:4d:b7:6c:be"
	nodes[47].Infranet().Mac = "10:70:fd:45:49:0c"
	nodes[47].NICs[3].Ports[0].Mac = "08:c0:eb:ee:67:36"
	// end macs
}

func idealCabling(tb *Builder) {

	// Node connections

	for _, x := range nodes {

		// management network

		// Connections are made through the Builder `Connect` method. This
		// creates and stores a cable connection in the Builder's state that is
		// emitted as XIR connections when a model is generated by the builder.
		// If breakout cables are in use, there is also a `Breakout` method
		// available.

		// Connections are made between ports on a node that are
		// nested within a network interface card (NIC) object inside the XIR
		// resource. A number of convinence functions on XIR `Resource` objects
		// are availbale for selecting the ports. The `Next<X>` family of
		// functions iterates a subset of available ports and returns the first
		// one that does not already have a cable attached. For example
		// `NextSwp` only consideres switch ports (these are the ports on a
		// switch in the data plane, as opposed to things like management ports
		// that are not connected to the ASIC). `NextSwpG` further constrains
		// `NextSwp` to consider only ports with the provided number of gigabits
		// per second. There are many other helper functions of this sort in the
		// XIR base library.
		tb.Connect(
			x.NICs[1].Ports[0],
			mleaf.NextSwp(),
			RJ45(),
			Product("Generic", "Cat6 RJ45 Cable", "GENERIC_CAT6"),
		)

		// infranet

		tb.Connect(
			x.NICs[2].Ports[0],
			ileaf.NextSwpG(25),
			SFP28(),
			Product("FS", "25gbps fiber", "SFP28-25GSR-85/OM4-LC-LC-DX-FS-2M-PVC"),
		)

		// xpnet

		tb.Connect(
			x.NICs[3].Ports[0],
			xleaf.NextSwp(),
			SFP28(),
			Product("FS", "100gbps fiber", "QSFP28-SR4-100G/12FMTPOM4"),
		)

	}

	// Infrastructure connections

	for _, x := range infrapods {

		// mangement network
		tb.Connect(
			x.NICs[1].Ports[0],
			mleaf.NextSwpG(10), //using SFPP/RJ45 adapter
			RJ45(),
			Product("Generic", "Cat6 RJ45 Cable", "GENERIC_CAT6"),
		)

		// infranet

		tb.Connect(
			x.NICs[2].Ports[0],
			ileaf.NextSwpG(100),
			Product("FS", "100gbps fiber", "QSFP28-SR4-100G/12FMTPOM4"),
		)

	}

	// Ops connections (missing opsserver in xir)
	tb.Connect(
		ops.NICs[2].Ports[0],
		mleaf.NextSwpG(10),
		SFP28(),
		Product("FS", "25gbps fiber", "SFP28-25GSR-85/OM4-LC-LC-DX-FS-2M-PVC"),
	)
}
