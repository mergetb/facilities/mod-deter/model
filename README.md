# ModDeter

ModDeter is a testbed facility with 48 physical servers in 12 chassis units with
a total node capacity of 6144. This is a single rack system with a management
switch, experiment infrastructure switch and experiment data plane switch. The
rack comes with two infrapod servers and an ops server.

![](ModDeter.png)

This repository contains the ModDeter testbed model. The code is split into two
parts. The model definition which is also an importable model library is in
[topo.go](topo.go). The model computer aided design (mCAD) program is in 
[cmd/mod-deter/main.go](cmd/mod-deter/main.go). You can build the mCAD program
as follows.

```
cd cmd/mod-deter
go build
```
See the help menus for what this mCAD program can do
```
$ ./mod-deter
```
```
Testbed CAD utility

Usage:
  mod-deter [command]

Available Commands:
  help        Help about any command
  list        List items in the model
  lom         List of materials
  save        Save the testbed model to XIR
  server      Run model server
  spec        Generate specs

Flags:
  -h, --help   help for mod-deter

Use "mod-deter [command] --help" for more information about a command
```

## Quick Specs
```
NODES
--------------------------------------------------------------------------------
MANUFACTURER    MODEL                                    SKU                            QUANTITY
Gigabyte        Custom Mod-Deter Server                  MDSERVER/CX22885g-NVMe4-E8     48
+AMD            64C 2.0Ghz 256mb 200W                    Epyc 7702                      2
+Gigabyte       Platform Certified 16GB Memory module    GB16                           16
+Intel          Dual Port 25gbps OCP 3.0                 E810-XXVDA2                    1
+Intel          Dual Port 100gbps PCIe 4.0 x 16          E810-CQDA2                     1
+Intel          1024GB M.2 NVMe x4 22x110mm              DC P4511                       1
+Kioxia         1.6TB 3DWPD U.2 Gen4 NVMe                CD6-V-1600                     6

Gigabyte        Custom Infrapod Server                   INFRASERVER/CX1265g-NVMe-E8    2
+AMD            32C 2.8Ghz 256mb 3200Mhz 225W            Epyc 7543                      2
+Gigabyte       Platform Certified 16GB Memory module    GB16                           32
+Mellanox       Dual port 100gbps ConnectX6              MCX653106A-ECAT                1
+Intel          4TB U.2 15mm NVMe                        DC P4510-4000                  2
+Intel          1024GB M.2 NVMe x4 22x110mm              DC P4511                       1

Gigabyte        Custom Ops Server                        OPSSERVER/CX1165g-NVMe4-E8     1
+AMD            16C 3.0Ghz 128mb 3200Mhz 155W            Epyc 7313P                     1
+Gigabyte       Platform Certified 16GB Memory module    GB16                           16
+Intel          Quad Port 10/25gbps PCIe 4.0x16          E810-XXVDA4                    1
+Micron         480GB M.2 NVMe x4 22x80mm                M7300                          2
+Kioxia         3.2TB 3DWPD U.2 Gen4 NVMe                CD6-V-3200                     2



SWITCHES
--------------------------------------------------------------------------------
MANUFACTURER    MODEL                                         SKU              QUANTITY
EdgeCore        52 port 1G/10G switch                         AS4610-52T       1
Mellanox        48x 25gbps + 12x 100gbps Spectrum-2 switch    MSN3420-CB2FC    1
Mellanox        64x 100gbps Spectrum-3 switch                 MSN4600-CS2RC    1


CABLES
--------------------------------------------------------------------------------
MANUFACTURER    MODEL              SKU                                      QUANTITY
Generic         Cat6 RJ45 Cable    GENERIC_CAT6                             50
FS              25gbps fiber       SFP28-25GSR-85/OM4-LC-LC-DX-FS-2M-PVC    49
FS              100gbps fiber      QSFP28-SR4-100G/12FMTPOM4                50
```
