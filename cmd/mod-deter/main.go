package main

import (
	"gitlab.com/mergetb/facilities/mod-deter/model"
	"gitlab.com/mergetb/xir/v0.3/go/build"
)

func main() {
	build.Run(moddeter.Topo())
}
